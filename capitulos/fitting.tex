El objetivo de este capítulo es dar la definición de los ideales de Fitting y probar sus principales propiedades.
En el Capítulo \ref{capSup} utilizaremos estos ideales para encontrar la ecuación implícita y puntos múltiples de superficies parametrizadas.

Si tenemos un módulo finitamente generado $M$ sobre un anillo Noetheriano $R$ y conseguimos una presentación libre finitamente generada de $M$
\[ \varphi\colon F \to G , \]
 el $i$-ésimo ideal de Fitting de $M$ es el ideal generado por los menores de tamaño $\rg(G) - i$ de la matriz de $\varphi$ en cualquier par de bases. Para la buena definición de estos ideales debemos ver que no dependen de la presentación elegida, resultado que se conoce como el Lema de Fitting (Proposición \ref{fitt}). Presentamos dicho lema recién en la Sección \ref{secFitt1}, ya que para la demostración del mismo son necesarios los resultados que probamos en las primeras dos secciones.


Nos basamos en los Capítulos 19 y 20 de \cite{Eis94}, al igual que en el libro asumimos que todos los anillos son Noetherianos.

\section{Resoluciones libres y minimales}
%El objetivo de esta sección es dar la definición de resolución libre minimal.
\begin{mydef}
Dado un $R$-módulo $M$, una \emph{resolución} de $M$ es una sucesión exacta de $R$-módulos
\[ \Fc\colon \dots \ra F_n \xra{\varphi_n} F_{n-1} \ra \dots \ra F_1 \xra{\varphi_1} F_0 \xra{\varphi_0} M \ra 0.\]
A veces omitiremos la última parte
\[ \Fc\colon \dots \ra F_n \xra{\varphi_n} F_{n-1} \ra \dots \ra F_1 \xra{\varphi_1} F_0 ,\]
entendiendo que la homología es cero en todos los lugares excepto en $F_0$ y que $M = \coker \varphi_n$.

Si los $F_n$ son proyectivos decimos que $\Fc$ es una \emph{resolución proyectiva}, si los $F_n$ son libres $\Fc$ es una \emph{resolución libre}. Si además $R$ es un anillo graduado, los $F_n$ son $R$-módulos libres graduados y los mapas son homogéneos de grado cero decimos que es una \emph{resolución libre graduada} (notar que necesitamos además que $M$ sea un $R$-módulo graduado).

Si existe un $n_0$ tal que $F_n = 0$ para todo $n > n_0$ y $F_{n_0} \neq 0$ diremos que la resolución es \emph{finita} de longitud $n_0$.

Truncando una resolución en el primer paso se obtiene una \emph{presentación} de $M$, esto es un morfismo $F \xra{\varphi} G$ tal que $\coker \varphi = M$.
\end{mydef}

\begin{obs} \label{res_libre}
Todo $R$-módulo $M$ tiene una resolución libre, y si $R$ y $M$ son graduados existe una resolución libre graduada de $M$.

Sea $\{u_i\}_{i \in I}$ un conjunto de generadores de $M$, sea $F_0 = R^{(I)}$, junto con $\varphi_0$ el epimorfismo que manda los elementos de la base de $F_0$ en los $u_i$. Tenemos
\[ F_0 \xra{\varphi_0} M \ra 0 . \]
Repetimos para $\Ker \varphi_0$ obteniendo $ F_1 \xra{\tilde{\varphi}_1} \Ker \varphi_0 \ra 0$ y componemos con la inclusión
\begin{equation*}
\begin{tikzcd}[sep=small]
 & F_0 \ar{r}{\varphi_0} & M \ar{r} & 0 \\
F_1 \ar{ur}{\varphi_1} \ar[swap]{r}{\tilde{\varphi}_1} & \Ker \varphi_0 \ar[hookrightarrow]{u} \ar{r} & 0 \\
 & 0 \ar{u}
\end{tikzcd}.
\end{equation*}
Siguiendo obtenemos la resolución libre deseada. Notemos que si $R$ y $M$ son graduados podemos ir tomando los $F_i$ de manera que la resolución quede graduada.
\end{obs}

\begin{ej}
Como vimos en el Corolario \ref{coroKoszul} del capítulo anterior, si $x = x_1,\dots,x_n$ es una sucesión regular, entonces el complejo de Koszul $K(x)$ es una resolución libre de $R/(x_1,\dots,x_n)R$.
\end{ej}

\begin{mydef}
Un complejo
\[ \Fc\colon \dots \ra F_n \xra{\varphi_n} F_{n-1} \ra \dots \]
sobre un anillo local $(R,p)$ se dice \emph{minimal} si las flechas del complejo $\Fc \ot R/p $ son todas nulas, es decir $\varphi_n(F_n)\subseteq pF_{n-1}$ para todo $n$.
\end{mydef}

\begin{obs}
Si $\Fc$ es un complejo minimal de módulos libres finitamente generados cualquier matriz representando a $\varphi_n$ tiene todas sus entradas en $p$.
\end{obs}

\begin{ej}
Si $x = (x_1,\dots,x_n) \in R^n$ con $x_i \in p$ para todo $i$, entonces $K(x)$ es un complejo minimal.
Recordemos que para cada $n$ el diferencial de $K(x)$ en el lugar $n$ manda los generadores $e_{i_1} \wedge \dots \wedge e_{i_k}$ en $\sum_{j=1}^k (-1)^{j-1} \ x_{i_j} \, e_{i_1} \wedge \dots \wedge \widehat{e_{i_j}} \wedge \dots \wedge e_{i_k}$, por lo que su imagen cae en $p K(x)_{n-1}$.
\end{ej}

Para obtener una resolución libre de $M$ podríamos proceder como en la Observación \ref{res_libre} tomando en cada paso conjuntos de generadores minimales. Así construiríamos una resolución libre que podríamos haber llamado minimal. Sin embargo esto no resulta muy útil en general, ya que los conjuntos de generadores minimales no suelen tener buenas propiedades de unicidad. Por ejemplo el cardinal de un conjunto minimal de generadores no está bien definido (en los enteros $(5) = (10,15)$). El siguiente lema nos dice que sobre un anillo local o graduado, donde gracias a la Proposición \ref{generadores} el cardinal de un conjunto minimal de generadores está bien definido, las resoluciones obtenidas de esta manera resultan ser minimales en nuestro sentido original. En particular tenemos que siempre existen resoluciones minimales y un método para construirlas.

\begin{lema} \label{lema_minim}
Dada una resolución libre
\[ \Fc\colon \dots \ra F_n \xra{\varphi_n} F_{n-1} \ra \dots \xra{\varphi_1} F_0 \]
sobre un anillo local, son equivalentes:
\begin{enumerate}
\item $\Fc$ es un complejo minimal.
\item Para cada $n$ la imagen por la proyección $F_{n-1} \to \coker \varphi_n$ de cada base de $F_{n-1}$ es un conjunto de generadores minimal de $\coker \varphi_n$.
%\item Para cada $n$ la clase de cualquier base de $F_{n-1}$ resulta ser un conjunto de generadores minimal de $\coker \varphi_n$.
\end{enumerate}
\end{lema}

\begin{proof}
Sea $(R,p)$ el anillo local y llamemos $\varphi_0$ al morfismo $F_0 \to \coker \varphi_1$.

2.\ $\implies$ 1. Sea $n \geq 0$, supongamos que toda base $\{u_1,\dots,u_r\}$ de $F_{n-1}$ va a un conjunto minimal de generadores $\{\pi(u_1),\dots,\pi(u_r)\}$ de $\coker \varphi_n$ y mostremos que $\im \varphi_n \subseteq p F_{n-1}$. Consideremos
\begin{equation*}
\begin{tikzcd}[column sep=small]
F_{n-1} \ar{r}{\pi} \ar[swap]{d}{r} & \coker \varphi_n \ar{r}{q} & \coker \varphi_n / p (\coker \varphi_n) \\
F_{n-1} / p F_{n-1} \ar[swap,dashed]{urr}{f}
\end{tikzcd}.
\end{equation*}
Dado que $p F_{n-1} \subseteq q \pi$, existe una (única) $f$ que hace conmutar el diagrama. Sea $\{u_1,\dots,u_r\}$ una base del módulo libre $F_{n-1}$. Por un lado, el Lema de Nakayama nos dice que $\{r(u_1),\dots,r(u_r)\}$ es una base del espacio vectorial $F_{n-1} / p F_{n-1}$. Al mismo tiempo, por hipótesis $\{u_1,\dots,u_r\}$ va a parar vía $\pi$ a un conjunto de generadores minimales de $\coker \varphi_n$, que nuevamente por el Lema de Nakayama va a una base $\{q\pi(u_1),\dots,q\pi(u_r)\}$ del espacio vectorial $\coker \varphi_n / p (\coker \varphi_n)$. Por la conmutatividad del diagrama tenemos $q\pi(u_i) = fr(u_i)$. O sea que $f$ manda la base $\{r(u_1),\dots,r(u_r)\}$ de $F_{n-1} / p F_{n-1}$ en la base $\{q\pi(u_1),\dots,q\pi(u_r)\}$ de $\coker \varphi_n / p (\coker \varphi_n)$, por lo que es un isomorfismo de espacios vectoriales.
Sea $a \in \im \varphi_n$,
\begin{align*}
f(r(a)) &= q\pi(a) \\
 &= q(0) = 0.
\end{align*}
Como $f$ es monomorfismo $r(a) = 0$. Luego $a \in p F_{n-1}$. O sea $\im \varphi_n \subseteq p F_{n-1}$ como queríamos.

1.\ $\implies$ 2. Supongamos que $\im \varphi_n \subseteq p F_{n-1}$. Igual que antes consideremos el epimorfismo $ f\colon F_{n-1} / p F_{n-1} \to \coker \varphi_n / p \, \coker \varphi_n$. Si $f(r(a)) = 0$, entonces $q(\pi(a)) = 0$. Luego $\pi(a) \in p \, \coker \varphi_n$, por lo que $r(a) = 0$. O sea que $f$ es un isomorfismo de espacios vectoriales. Sea $\{u_1,\dots,u_r\}$ una base de $F_{n-1}$. Por el Lema de Nakayama, $\{r(u_1),\dots,r(u_r)\}$ es una base de $F_{n-1} / p F_{n-1}$, que vía $f$ va a una base $\{q\pi(u_1),\dots,q\pi(u_r)\}$ de $\coker \varphi_n / p (\coker \varphi_n)$. Nuevamente usando el Lema de Nakayama tenemos que $\{\pi(u_1),\dots,\pi(u_r)\}$ es un conjunto minimal de generadores de $\coker \varphi_n$.
\end{proof}

\begin{coro} \label{existencia}
Si $R$ es un anillo local o graduado y $M$ un $R$-módulo, entonces existe una resolución minimal de $M$.
\end{coro}

\begin{proof}
Construimos una resolución libre de $M$ procediendo como en la observación \ref{res_libre}, pero en cada paso tomando un conjunto minimal $\{u^n_1,\dots,u^n_{r_n}\}$ de generadores de $\Ker \varphi_{n-1}$. Tenemos
\[ \coker \varphi_n = F_{n-1} / \im \varphi_n = F_{n-1} / \Ker \varphi_{n-1} \simeq \Ker \varphi_{n-2} . \]
La base $\{u^n_1,\dots,u^n_{r_n}\}$ de $F_n=R^{r_n}$ va a un conjunto minimal de generadores $\Ker \varphi_{n-1} \simeq \coker \varphi_n$. Por el Lema \ref{lema_minim} la resolución construida resulta ser minimal.
\end{proof}

\section{La unicidad de las resoluciones minimales}
Lo opuesto a un complejo minimal es un complejo trivial, el cual definimos a continuación.
\begin{mydef}
Un complejo se dice \emph{trivial} si es suma directa de complejos de la forma
\[ 0 \ra R \xra{1} R \ra 0 \]
y sus trasladados
\end{mydef}

\begin{ej}
El complejo
\[ 0 \ra R \xra{\binom{1}{0}} R^2 \xra{(0 \: 1)} R \ra 0 \]
es isomorfo a
\begin{equation*}
\begin{tikzcd}[row sep=0, column sep=small]
0 \ar{r} & R \ar{r}{1} & R \ar{r} & 0 \\
 & \op & \op & \op \\
 & 0 \ar{r} & R \ar{r}{1} & R \ar{r} & 0, \\
\end{tikzcd}
\end{equation*}
luego es un complejo trivial.
\end{ej}

Los complejos triviales tienen homología nula, por lo que si $\Gc$ es un complejo trivial y $\Fc$ es una resolución de $M$, entonces  $\Fc \op \Gc$ es una resolución de $M$. Este es el motivo más simple de no unicidad de las resoluciones libres. En el Teorema \ref{unicidad} veremos que cuando estamos trabajando sobre un anillo local es el único motivo.

\begin{lema} \label{triv}
Dado $R$ anillo local, toda
\[ \Hc\colon \dots \xra{\varphi_{n+1}} H_n \xra{\varphi_n} \dots \xra{\varphi_2} H_1 \xra{\varphi_1} H_0 \ra 0 \]
sucesión exacta de módulos libres sobre $R$ es un complejo trivial.
\end{lema}

\begin{proof}
El $R$-módulo $H_0$ es libre, luego proyectivo, y la sucesión exacta corta
\[ 0 \ra \Ker \varphi_1 \ra H_1 \xra{\varphi_1} H_0 \ra 0 \]
se parte. Es decir
\[ H_1 = H_0 \op \Ker \varphi_1. \]
Sea $H'_1 = \Ker \varphi_1 = \im \varphi_2$, notemos que $H_0 \op H'_1 \xra{\varphi_1} H_0$ es la proyección al primer factor. Luego $\Hc$ es suma directa del complejo trivial
\[ \Hc_1\colon 0 \ra H_0 \xra{1} H_0 \ra 0 \]
y
\[ \Hc'\colon \dots \ra H_n \ra \dots \ra H_2 \ra H'_1 \ra 0, \]
donde $\Hc'$ resulta de $\Hc$ reemplazando $H_1$ por $H'_1 = \im \varphi_2$.
Observemos que $H_1 = H_0 \op H'_1$ con $H_1$ libre, luego $H'_1$ es proyectivo (es sumando directo de un libre).
Por el Corolario \ref{nakaProy} del Lema de Nakayama todo módulo proyectivo finitamente generado sobre un anillo local es libre, luego $H'_1$ es libre y $\Hc'$ resulta ser un complejo de módulos libres.
Repitiendo obtenemos una descomposición $\Hc = \Hc_1 \op \Hc_2 \op \dots$ como suma directa de complejos triviales.
\end{proof}

\begin{lema}
Sea
\[ \Fc\colon \dots \xra{\varphi_{n+1}} F_n \xra{\varphi_n} \dots \xra{\varphi_1} F_0 \xra{\varphi_0} M \ra 0 \]
un complejo de $R$-módulos con los $F_i$ proyectivos (por ejemplo una resolución proyectiva de $M$) y sea
\[ \Gc\colon \dots \xra{\psi_{n+1}} G_n \xra{\psi_n} \dots \xra{\psi_1} G_0 \xra{\psi_0} N \ra 0 \]
una resolución de $N$. Para todo $\alpha\colon M \to N$ morfismo de $R$-módulos existe un $\tilde{\alpha}\colon \Fc \to \Gc$ morfismo de complejos que induce $\alpha$, que llamaremos levantado de $\alpha$. Además, si $\tilde{\beta}\colon \Fc \to \Gc$ es otro levantado de $\alpha$, entonces $\tilde{\alpha}$ y $\tilde{\beta}$ son homotópicos como complejos.
\end{lema}

\begin{proof}
Razonamos de manera inductiva.

\emph{Existencia}: Consideremos
\begin{equation*}
\begin{tikzcd}[sep=small]
 & F_0 \ar[dashed,swap]{ddl}{\alpha_0} \ar{d}{\varphi_0} & \\
 & M \ar{d}{\alpha} & \\
G_0 \ar{r}{\varphi_0} & N \ar{r} & 0.
\end{tikzcd}
\end{equation*}
Como $F_0$ es proyectivo existe $\alpha_0\colon F_0 \to G_0$ tal que $\psi_0\alpha_0 = \alpha\varphi_0$. Continuamos de manera análoga.
\begin{equation*}
\begin{tikzcd}[sep=small]
 & F_1 \ar[dashed,swap]{ddl}{\alpha_1} \ar{d}{\varphi_1} & \\
 & F_0 \ar{d}{\alpha_0} & \\
G_1 \ar{r}{\varphi_1} & N \ar{r}{\psi_0} & N
\end{tikzcd}
\end{equation*}
Como $\psi_0\alpha_0\varphi_1 = \alpha_0\varphi_0\varphi_1 = 0$, tenemos $\im (\alpha_0\varphi_1) \subseteq \Ker \psi_0 = \im \psi_1$. Luego existe $\alpha_1\colon F_1 \to G_1$ tal que $\psi_1\alpha_1 = \alpha_0 \varphi_1$. Seguimos inductivamente obteniendo un morfismo de complejos
\begin{equation*}
\begin{tikzcd}[sep=small]
\dots \ar{r} & F_n \ar{d}{\alpha_n} \ar{r} & \dots \ar{r} & F_1 \ar{d}{\alpha_1} \ar{r} & F_0 \ar{d}{\alpha_0} \ar{r} & M \ar{d}{\alpha} \ar{r} & 0 \\
\dots \ar{r} & G_n \ar{r} & \dots \ar{r} & G_1 \ar{r} & G_0 \ar{r} & N \ar{r} & 0, \\
\end{tikzcd}
\end{equation*}
que notamos $\tilde{\alpha}\colon \Fc \to \Gc$.

\emph{Unicidad a menos de homotopía}:
Supongamos que tenemos $\tilde{\alpha}, \tilde{\beta}\colon \Fc \to \Gc$ dos levantados de $\alpha\colon M \to N$. Queremos morfismos $h_i\colon F_i \to G_{i+1}$ tales que $\alpha_i - \beta_i = h_{i+1}\varphi_i + \psi_{i+1}h_i$
\begin{equation*}
\begin{tikzcd}
\dots \ar{r} & F_2 \ar{d}[yshift=1.3ex]{\alpha_2 - \beta_2} \ar{r}{\varphi_2} & F_1 \ar[dashed]{dl}{h_1} \ar{d}[yshift=1.3ex]{\alpha_1 - \beta_1} \ar{r}{\varphi_1} & F_0 \ar[dashed]{dl}{h_0} \ar{d}[yshift=1.3ex]{\alpha_0 - \beta_0} \ar{r}{\varphi_0} & M \ar[dashed]{dl}{h_{-1}} \ar{d}{0} \ar{r} & 0 \\
\dots \ar{r} & G_2 \ar{r}[swap]{\psi_2} & G_1 \ar{r}[swap]{\psi_1} & G_0 \ar{r}[swap]{\psi_0} & N \ar{r} & 0. \\
\end{tikzcd}
\end{equation*}
Tomamos $h_{-1}=0$ y empezamos con $h_0$.
\begin{align*}
\psi_0 (\alpha_0 - \beta_0) &= \psi_0\alpha_0 - \psi_0\beta_0 \\
 &= \alpha\varphi_0 - \alpha\varphi_0 \\
 &= 0.
\end{align*}
Luego $\im (\alpha_0 - \beta_0) \subseteq \Ker \psi_0 = \im \psi_1$. Como $F_0$ es proyectivo existe $h_0\colon F_0 \to G_1$ tal que $\varphi_1h_0 = \alpha_0 - \beta_0$.

Sigamos un paso más, queremos $\alpha_1 - \beta_1 = h_0\varphi_1 + \psi_2h_1$.
\begin{align*}
\psi_1(\alpha_1 - \beta_1) &= \psi_1\alpha_1 - \psi_1\beta_1 \\
 &= \alpha_0\varphi_1 - \beta_0\varphi_1 \\
 &= (\alpha_0 - \beta_0)\varphi_1 \\
 &= \psi_1h_0\varphi_1.
\end{align*}
Luego $\alpha_1 - \beta_1 - h_0\varphi_1 \in \Ker \psi_1 = \im \psi_2$, por lo que existe $h_1\colon F_1 \to G_2$ tal que $\psi_2h_1 = \alpha_1 - \beta_1 - h_0\varphi_1$,
\begin{equation*}
\begin{tikzcd}[sep=small]
 & F_1 \ar[swap]{dl}{h_1} \ar{d}{\alpha_1 - \beta_1 - h_0\varphi_1} \\
G_2 \ar{r}[swap]{\varphi_2} & G_1.
\end{tikzcd}
\end{equation*}
O sea que $\alpha_1 - \beta_1 = h_0\varphi_1 + \psi_2h_1$.
Repetimos para obtener los $h_i\colon F_i \to G_{i+1}$ que nos dan la homotopía de complejos entre $\tilde{\alpha}\colon \Fc \to \Gc$ y $\tilde{\beta}\colon \Fc \to \Gc$.
\end{proof}

\begin{teo} \label{unicidad}
Sea $R$ un anillo local, $M$ un $R$-módulo finitamente generado y $\Fc$ una resolución libre minimal de $M$. Entonces cualquier otra resolución libre de $M$ es isomorfa a $\Fc\op\Hc$ con $\Hc$ complejo trivial. En particular, a menos de isomorfismo, hay una única resolución minimal de $M$.
\end{teo}

\begin{proof}
Tenemos
\[ \Fc\colon \dots \ra F_n \ra \dots \ra F_0 \xra{\varphi_0} M \ra 0 \]
una resolución libre minimal y sea
\[ \Gc\colon \dots \ra G_n \ra \dots \ra G_0 \xra{\psi_0} M \ra 0 \]
otra resolución de $M$. Sean $\alpha\colon \Fc \to \mathcal{G}$ y $\beta\colon \mathcal{G} \to \mathcal{F}$ levantados de $\text{id}_M$, $\beta\alpha\colon \mathcal{G} \to \mathcal{F}$ es un levantado de $\text{id}_M$ homotópicamente equivalente a $\text{id}_{\mathcal{F}}$. O sea que existen $s_i\colon F_i \to F_{i+1}$ tales que $1 - \beta_i\alpha_i = s_{i-1}\varphi_i + \varphi_{i+1}s_i$. Como $\mathcal{F}$ es minimal tenemos que $(1-\beta_i\alpha_i)(F_i) \subseteq pF_i$. Luego $\text{det}(\beta_i\alpha_i) \equiv 1 \ (\text{mod p})$. En particular $\text{det}(\beta_i\alpha_i) \neq 0$, por lo que $\beta_i\alpha_i$ es un automorfismo de $\Fc$. Reemplazando $\beta$ por $(\beta\alpha)^{-1}\beta$, podemos asumir que $\beta\alpha = 1$. Para cada $i$ sea $H_i = \coker \alpha_i$, tenemos
\begin{equation*}
\begin{tikzcd}[sep=small]
0 \ar{r} & F_i \ar{r}{\alpha_i} & G_i \ar[bend left=50]{l}{\beta_i} \ar{r}{\pi_i} & H_i \ar{r} & 0.
\end{tikzcd}
\end{equation*}
Luego $G_i \simeq F_i \op H_i$, con $G_i$ y $F_i$ libres, luego $H_i$ también lo es. Consideremos
\begin{equation*}
\begin{tikzcd}[sep=small]
G_i \ar{r}{\psi_i} \ar{d} & G_{i-1} \ar{r}{\pi_{i-1}} & G_{i-1} / \, \im \alpha_{i-1} \\
G_i / \, \im \alpha_i \ar[dashed,swap]{urr}{\nu_i} .
\end{tikzcd}
\end{equation*}
Notemos que $\pi_{i-1}\psi_i\alpha_i(x) = \pi_{i-1}\alpha_{i-1}\varphi_{i-1}(x) = 0$, luego $\im \alpha_i \subseteq \Ker \pi_{i-1}\psi_i$ y existe $\nu_i\colon H_i \to H_{i-1}$. Además $\nu_i \, \nu_{i+1}([x]) = \nu_i [ \psi_{i+1} (x) ] = \psi_i \, \psi_{i+1} (x) = 0$. Tenemos un complejo de módulos libres
\[ \Hc\colon \dots \ra H_n \xra{\nu_n} H_{n-1} \ra \dots \]
y una sucesión exacta corta de complejos
\begin{equation*}
\begin{tikzcd}[sep=small]
0 \ar{r} & \Fc \ar{r}{\alpha} & \Gc \ar[bend left=50]{l}{\beta} \ar{r}{\pi} & \Hc \ar{r} & 0.
\end{tikzcd}
\end{equation*}
Luego el complejo $\Gc$ es isomorfo a $\Fc \op \Hc$, por lo que $H(\Gc) \simeq H(\Fc) \op H(\Hc)$. Pero $\alpha\colon \Fc \to \Gc$ induce un isomorfismo en la homología. Luego $\Hc$ es exacto y por lo visto en el lema \ref{triv}, $\Hc$ resulta ser un complejo trivial.
\end{proof}

\section{Ideales de Fitting} \label{secFitt1}
En esta sección definimos los ideales de Fitting de un módulo finitamente generado y damos sus principales características.

\begin{mydef}
Si $F$ y $G$ módulos libres finitamente generados y $\varphi\colon F \to G$ es un morfismo de $R$-módulos, llamamos $I_r\varphi$ a la imagen del morfismo %%$\phi$ inducido por $\wedge^r\varphi\colon \wedge^rF \to \wedge^rG$,
\begin{align*}
\wedge^rF\ot\wedge^rG^* &\xra{\phi} R \\
x_1\wedge\dots\wedge x_r \ot \alpha_1\wedge\dots\wedge \alpha_r &\mapsto \text{det}(\alpha_i \, \varphi(x_j))_{ij}.
\end{align*}
\end{mydef}
Eligiendo bases de $F$ y de $G$, $\varphi$ queda representada por la matriz $M(\varphi)$. A continuación veremos que $I_r\varphi$ es el ideal de $R$ generado por los menores (determinantes de las submatrices) de tamaño $r$ de la matriz $M(\varphi)$. Tomamos por convención que el menor de tamaño $0$ es $1$, luego $I_0\varphi = R$, y tomamos $I_r\varphi = R$ para todo $r \leq 0$. Observemos que con esta definición los ideales de Fittng no dependen de la elección de las bases de $F$ y de $G$.

\begin{lema}
Sean $B_1 = \{x_1,\dots,x_m\}$ una base de $F$, $B_2 = \{y_1,\dots,y_n\}$ una base de $G$ y sea $M$ la matriz de $\varphi$ en estas bases. Entonces $I_r\varphi$ es el ideal de $R$ generado por los menores de tamaño $r$ de $M$.
\end{lema}
\begin{proof}
Llamemos $J_r \subseteq R$ al ideal generado por los menores de tamaño $r$ de M, queremos ver que $J_r = I_r\varphi$. Sea $m_{i_1\dots i_r, j_1\dots j_r}$ el determinante de la submatriz de $M$ que se obtiene quedándose con las filas $i_1,\dots,i_r$ y las columnas $j_1,\dots,j_r$. Consideremos ${\alpha_1,\dots,\alpha_n}$ la base dual de $\{y_1,\dots,y_n\}$ y notemos que $\alpha_i \, \varphi (x_j) = M_{ij}$. Luego
\[ m_{i_1\dots i_r, j_1\dots j_r} = \phi (x_{i_1}\wedge\dots\wedge x_{i_r} \ot \alpha_{j_1}\wedge\dots\wedge\alpha_{j_r}), \]
por lo que $J_r \subseteq I_r\varphi$. Veamos la otra inclusión. Sea $a \in I_r\varphi$.
\begin{align*}
a &= \phi\left(a_1\wedge\dots\wedge a_r\ot f_1\wedge\dots\wedge f_r\right) \\
 &= \phi\left(\sum_{k=1}^m a^1_kx_k\wedge\dots\wedge \sum_{k=1}^m a^r_kx_k\ot \sum_{k=1}^n b^1_k\alpha_k\wedge\dots\wedge \sum_{k=1}^n b^r_k\alpha_k\right) \\
 &= \phi\left(\sum c_{i_1\dots i_r, j_1\dots j_r} x_1\wedge\dots\wedge x_r\ot\alpha_1\wedge\dots\wedge\alpha_r\right) \\
 &= \sum c_{i_1\dots i_r, j_1\dots j_r} \phi\left(x_1\wedge\dots\wedge x_r\ot\alpha_1\wedge\dots\wedge\alpha_r\right).
\end{align*}
Como observamos antes $\phi\left(x_1\wedge\dots\wedge x_r\ot\alpha_1\wedge\dots\wedge\alpha_r\right)$ es el menor que se obtiene quedándose con las filas $i_1,\dots,i_r$ y las columnas $j_1,\dots,j_r$ de $M$. Luego $a \in J_r$, como queríamos.
\end{proof}

\begin{obs} \label{obsStrat}
Usando la fórmula de Laplace para desarrollar por filas o columnas los menores de la matriz $M(\varphi)$ se ve que si $r \leq s$, entonces $I_s\varphi \subseteq I_r\varphi$. 
\end{obs}

La siguiente proposición es necesaria para la buena definición de los ideales de Fitting.
\begin{prop}[Lema de Fitting] \label{fitt}
Sea $M$ un $R$-módulo finitamente generado y sean $\varphi\colon F \to G$ y $\varphi'\colon F' \to G'$ dos presentaciones de $M$ libres finitamente generadas, con $G$ y $G'$ de rango $r$ y $r'$ respectivamente. Para cada $i \in \ZZ_{\geq 0}$ tenemos $I_{r-i}\varphi = I_{r'-i}\varphi'$.
\end{prop}
\begin{proof}
Como vimos en el Corolario \ref{loc} dos ideales son iguales si y sólo si coinciden en toda localización, así que podemos asumir que $R$ es local. En ese contexto, por el Corolario \ref{existencia} sabemos que existe una resolución minimal $\Fc$. Llamemos $\varphi\colon F \to G$ al primer morfismo de $\Fc$, es decir a la presentación minimal. Dada $\varphi'\colon F' \to G'$ otra presentación, digamos que viene de una resolución $\Gc$, por el Teorema \ref{unicidad} tenemos que $\Gc \simeq \Fc \op \Hc$ con $\Hc$ complejo trivial. Es decir
\[ \Gc \simeq \Fc \op \Hc_1 \op \Hc_2 \op \dots , \]
donde los $\Hc_i$ son todos complejos isomorfos a trasladados del complejo
\[ 0 \ra R \xra{1} R \ra 0 . \]
Luego $\varphi'$ puede ser expresada de la forma
\begin{equation*}
\varphi' = \left( 
 \begin{array}{c|c|c}
  \varphi & 0 & 0 \\
\hline
  0 & 1 & 0
 \end{array} \right) ,
\end{equation*}
donde $1$ es la matriz identidad de $p \times p$. Debemos ver que $I_j\varphi = I_{j+p}\varphi'$ para todo $j$. Es claro que $I_j\varphi \subseteq I_{j+p}\varphi'$. Veamos la otra contención. Sea $m'$ un menor de tamaño $j+p$ de $\varphi'$. Notar que $m' = m$, donde $m$ es un menor de tamaño $i$ de $\varphi$ con $i \geq j$. Luego $I_{j+p}\varphi' \subseteq I_i\varphi' \subseteq I_j\varphi'$.
\end{proof}

Finalmente estamos en condiciones de definir los ideales de Fitting.

\begin{mydef}
Sea $M$ un $R$-módulo finitamente generado definimos el \emph{i-ésimo ideal de Fitting de $M$} como
\[ \fitt_i(M) = I_{r-i} \varphi , \]
donde $\varphi\colon F \to G$ es cualquier presentación libre finitamente generada de $M$ y $r$ es el rango de $G$.
\end{mydef}

Veamos a continuación algunas propiedades de los ideales de Fitting.
En primer lugar notemos que por la Observación \ref{obsStrat} tenemos la siguiente cadena de inclusiones
\begin{equation} \label{cadena}
\fitt_0(M) \subset \fitt_1(M) \subset \dots \subset \fitt_r(M) = R \, .
\end{equation}
Además los ideales de Fitting conmutan con el cambio de base.
\begin{lema}
Para todo morfismo de anillos $R \to S$,
\[ \fitt_j(M \ot_R S) = (\fitt_jM) S. \]
\end{lema}

\begin{proof}
Sea
\[ F \xra{\varphi} G \ra M \ra 0 \]
una presentación libre de $M$. Dado que el producto tensorial es exacto a derecha, tensorizando con $S$ tenemos que
\[ F \ot_R S \xra{\varphi\ot 1} G \ot_R S \ra M \ot_R S \ra 0 \]
es una presentación libre de $M \ot_R S$. Observemos que tomando bases $B_1$ de $F$ y $B_2$ de $G$ como módulos sobre $R$, si las vemos dentro de $S$, $B_1$ y $B_2$ son bases de $F$ y $G$ respectivamente como módulos sobre $S$. La afirmación sale como consecuencia de que en estas bases los morfismos $\varphi$ y $\varphi\ot 1$ quedan representados por la misma matriz $M$, cuyas entradas son elementos del anillo $R$, y de que los ideales de Fitting no dependen de la presentación ni de las bases elegidas.
\end{proof}

A continuación vemos que el ideal de Fitting $\fitt_j(M)$ mide la obstrucción para generar $M$ con $j$ elementos. 
\begin{prop}
Sea $(R,p)$ anillo local y $M$ un $R$-módulo. Entonces $M$ puede ser generado por $j$ elementos si y sólo si $\fitt_j M = R$.\end{prop}

\begin{proof}
Sea $\varphi\colon F \to G$ una presentación minimal, o sea $M=\coker\varphi$ y cualquier matriz que representa a $\varphi$ tiene todas sus entradas en $p$. Sea $r = \text{rg}\,(G)$, notemos que cualquier conjunto minimal de generadores de $M$ tiene cardinal $r$. Calculando $\fitt_j(M)$, vemos que $\fitt_j(M) \subseteq p$ si y sólo si $j<r$, ya que tomamos una resolución minimal de $M$. Luego $\fitt_j(M) = R$ si y sólo si $j \geq r$ y esto ocurre si y sólo si $M$ puede ser generado por $j$ elementos.
\end{proof}

\begin{coro}
Dados $R$ un anillo y $M$ un $R$-módulo
\[ V(\fitt_j M) = \{ q \in \text{Spec}\, R \mid M_q \ \text{no puede ser generado por $j$ elementos}\}. \]
\end{coro}

\begin{proof}
Sea $q \in \text{Spec}\, R$ y consideremos la localización de $R$ en $q$. En el anillo local $(R_q,q)$ podemos usar la proposición anterior. Tenemos que $M_q$ no puede ser generado por $j$ elementos si y sólo si $\fitt_j M_q \subseteq q$. Pero $\fitt_j M_q = \fitt_j (R_q \ot_R M) = (\fitt_j M)R_q \subseteq q$ si y sólo si $\fitt_j M \subseteq q$ si y sólo si $q \in V(\fitt_j M)$.
\end{proof}

Necesitaremos la siguiente proposición cuando probemos las propiedades de las matrices de representación en el Capítulo \ref{capSup}. Lo más importante que de ella se desprende es que el anulador de $M$ y el ideal de Fitting inicial $\fitt_0 M$ tienen el mismo radical, como se menciona en la Observación \ref{obsRadical} más abajo.
\begin{prop} \label{sop}
Para cualquier $R$-módulo $M$ valen:
\begin{enumerate}%[label=\alph*\rm)]
\item $\fitt_0 M \subset \text{Ann}\, M$.
\item Para todo $j > 0$ tenemos $(\text{Ann}\, M) \fitt_j M \subset \fitt_{j-1} M$. Y si $M$ puede ser generado con $n$ elementos, entonces $(\text{Ann}\, M)^n \subset \fitt_0 M$.
\end{enumerate}
\end{prop}

\begin{proof}
Sea $\varphi\colon F \to R^n$ una presentación de $M$.

1. $\fitt_0 M = I_n \varphi$. Si $\varphi'$ es una $n \times n$ submatriz de $\varphi$, entonces $M$ es isomorfo a $M' = \coker \varphi'$, luego $\text{Ann}\, M' = \text{Ann}\, M$. Así que basta considerar el caso en el que $G=R^n$ y $\varphi$ es una matriz cuadrada. Sea $\psi$ la matriz de cofactores de $\varphi$ y notemos $d = \text{det}(\varphi)$. Tenemos el siguiente diagrama conmutativo
\begin{equation*}
\begin{tikzcd}
R^n \ar{r}{\varphi} & R^n \ar{r}{f} \ar{d}{d 1_{R^n}} \ar[swap]{dl}{\psi} & M \ar{r} \ar{d}{d 1_M} & 0 \\
R^n \ar{r}{\varphi} & R^n \ar{r}{f} & M \ar{r} & 0 
\end{tikzcd}
\end{equation*}
Sea $a \in M$. Como $f$ es un epimorfismo existe $b \in R^n$ tal que $f(b)=a$. Tenemos $da = df(b) = f\varphi\psi (b) = 0$, ya que $f\varphi = 0$. Luego $d 1_M = 0$, por lo que $\fitt_0 M \subset \text{Ann}\, M$.

2. Debemos ver que si $a \in \text{Ann}\, M$ y $\varphi'$ es una $(j-1)\times(j-1)$ submatriz de $\varphi$ con $j-1 < n$, entonces $a \cdot \text{det}(\varphi') \in I_j(\varphi)$. El morfismo $\varphi + a1_{R^n}\colon G \op R^n \to R^n$, donde $a 1_{R^n}\colon R^n \to R^n$ es la multiplicación por $a$, es otra presentación de $M$. Sea $r = \text{rg}(G)$; en términos de matrices $\varphi + a1_{R^n}$ está representada por una matriz de tamaño $n \times (n+r)$ de la forma
\begin{equation*}
\varphi + a1_{R^n} = \left( 
 \begin{array}{cc|ccc|ccccc}
 \ \varphi' &	& 	&				&	& a	& 		& 		& \\
 &				&	&				&	&		& 	a	& 		& \\
\cline{1-5}
 &				&	& \varphi	&	&		&		& a	& \\
 &				&	&				&	&		&		&		& a
 \end{array} \right) .
\end{equation*}
Sea $\varphi''$ la submatriz de $\varphi + a1_{R^n}$ que se obtiene tomando $\varphi'$ y una fila que no está en $\varphi'$ y una columna de $a1_{R^n}$. Tenemos $\varphi'' = \varphi' \op a$, luego $\text{det}(\varphi'') = a \, \text{det}(\varphi')$. Por el lema de Fitting $\text{det}(\varphi'') \in I_j(\varphi + a1_{R^n})=I_j(\varphi)$, que es lo que queríamos ver.

Para la segunda parte de la afirmación, notemos que $\fitt_n M = R$. Iterando obtenemos
\[ (\text{Ann}\, M)R \subseteq \fitt_{n-1} M , \]
\[ (\text{Ann}\, M)^2 \subseteq (\text{Ann}\, M)\fitt_{n-1} M \subseteq \fitt_{n-2} M , \]
\[ \vdots \]
\[ (\text{Ann}\, M)^n \subseteq (\text{Ann}\, M)\fitt_1 M \subseteq \fitt_0 M . \]
%\begin{align*}
%(\text{Ann}\, M)R &\subseteq \fitt_{n-1} M , \\
%(\text{Ann}\, M)^2 &\subseteq (\text{Ann}\, M)\fitt_{n-1} M \subseteq \fitt_{n-2} M , \\
% & \qquad \qquad \vdots \\
%(\text{Ann}\, M)^n &\subseteq (\text{Ann}\, M)\fitt_1 M \subseteq \fitt_0 M .
%\end{align*}
\end{proof}

\begin{coro}
Si $M$ es un $R$-módulo finitamente generado, entonces
\[ (\text{Ann}\, M)^n \subseteq \fitt_0 M \subset \text{Ann}\, M , \]
para algún $n$.
\end{coro}

\begin{obs} \label{obsRadical}
El corolario anterior nos dice que el ideal de Fitting inicial y el anulador de $M$ tienen el mismo radical. Luego $V(\text{Ann}\, M)$ y $V(\fitt_0 M)$ coinciden como conjuntos, aunque no como esquemas.
Como veremos en la Observación \ref{obsFittIm} del próximo capítulo, esto nos permite dotar a la imagen de $\phi$ de dos estructuras de esquema distintas: el esquema proyectivo definido por el ideal $\fitt_0 M$ que llamamos imagen de Fitting de $\phi$, y una segunda en la que no haremos foco, dada por el ideal $\text{Ann}\, M$ y conocida como scheme-theoretic image.
\end{obs}
